# tangods-struck

![5 - Production](https://img.shields.io/badge/Development_Status-5_--_production-green.svg)

Licence: GPLv3+

release 1.0.4

**Table of contents**

[[_TOC_]]

---

## ToDo list

- [x] From the excel source prepare a csv and generate the skeleton of a 
  device with attributes from there.
  - [ ] further than attribute name, provide label and description in the csv
  - [ ] enumeration attributes with a state/status pairs (machine/human 
    usage) with a list of allowed values attribute that can be used to 
    populate a combobox in the gui
- [x] procedure to set initial settings
  - [x] Device startup
  - [x] Card initialization
  - [x] Hot plug configuration for card reset

## prepare

### git submodules

After clone the repo, it is necessary to get the submodules:

```bash
git submodule init
git submodule update
```

There are two submodules in this project. One provides de 
[registers](https://gitlab.com/srgblnch-tangocs/struck/registers) in a 
```csv``` description and the other is the 
[codegenerator](https://gitlab.com/srgblnch-tangocs/struck/codegenerator),  
the tool to convert this description to code that is then used within the 
device server as well as it is used by the graphical interface.

### conda environment

Then it is also necessary to set up a conda environment.

```bash
conda env create -f environment.yml
```

In development environment use the ```environment-dev.yml``` file

This project needs access to a struck card provided by another python module 
(aka [python-struck](https://gitlab.com/srgblnch-tangocs/struck/python-struck)), 
see the readme in this dependency project, but at this stage you will have 
to install a python wheel in the current environment.

```bash
pushd $SRC/
git clone git@git.cells.es:controls/struck/python-struck.git
cd python-struck
conda env create -f environment.yml
conda activate python-struck
python setup.py bdist_wheel
popd
```

Then this distribution package can be installed in the tangods environment.

```bash
pushd $SRC/tangods-struck
conda env create -f environment.yml 
conda activate tangods-struck
pip install dist/struck-1.0.0-cp39-cp39-linux_x86_64.whl
```

In a final installation is good to define an export in a file like 
```/etc/profile.d/99-struck.sh```. Even more, this export can include all 
the struck card libraries.

```bash
export LD_LIBRARY_PATH=/home/alba/SIS/lib/libSIS830x:$LD_LIBRARY_PATH
```

### code generation

The repo already includes the sources of a generated device. So this step is 
only necessary in case of modifications in the csv file.

(FIXME): this is not working with python 3.9, use python2.7 until fix
```bash
python tangods_struck/codegenerator/codegenerator.py tangods_struck/registers/struck.csv tangods_struck/dllrf.py
```

## Installation

Like it has been made in the previous python module to access the card, the 
device can be installed in the appropriate conda environment.

Using the development environment, we will create the wheel and install it 
in the deployment environment.

```bash
conda env create -f environment-dev.yml
conda activate tangods-struck-dev
python setup.py bdist_wheel
```

Then this wheel can be installed in the same environment where the card 
access binding has been placed.

```bash
conda activate tangods-struck
pip install dist/tangods_struck-1.0.4-py3-none-any.whl
```

## Usage

Once the code prerequisites are full-filled, and this module has been 
installed in the appropriated environment, it can be launched. The package 
provide an entry point so from the environment ```dllrf -?``` can be called.

This tangods project includes also a launcher that can be placed in the 
Starter path that, using the appropriate environment launches the device server.

If there isn't a tango installation available, one can take a look on the 
docker-compose project to have [tango database as a service](https://gitlab.com/srgblnch-tangocs/docker-compose-tango-cs).

Also, from this project one can get two dependencies. From one side, the 
`tango-starter`. From another, and with the dependencies from the OS 
`openjdk-11-jre` and `liblog4j1.2-java` the `libtango-java` to have *jive* 
and *astor* in your installation.

### launch with tango-starter

There is a bash script that launches this device server in the directory 
`launcher` of this project. This file, with the appropriate permissions can 
be copied to the path where the starter looks for device server launchers 
(usually `/usr/lib/tango`).

The starter devices server installed from the package `tango-starter`, runs with 
the rights of the `tango` user. So the launcher with do a user change 
because all the environments has been made for the `alba` user. To do such 
thing, the `tango` user has to be allowed to do a `su` to the `alba` user.

One way to allow that is to add a file called ```/etc/sudoers.
d/tango_alba``` with the content ```tango ALL=NOPASSWD: /bin/su - alba``` to 
then, `tango` user can change to `alba` without auth requirement.

### delay the tango-starter service

The `tango-starter` package comes with a configuration based on _system V_ 
that can be used from _systemd_. But there isn't (or I didn't found) a way 
to establish a dependency of a service on a running container.

The point is that the starter could be launched before the container with 
the tango database is running. So for this reason, I propose a bad solution 
of modify the `/etc/init.d/tango-starter` and add the following lines to the 
`do_start()` function:

```bash
        # check if the tangodb container is up, total wait of 5 minutes
        i=0
        while [ `docker container ls | grep tangodb` -ne 0 ]; do
                if [[ "$i" == "10" ]]; then
                        break;
                fi
                /bin/sleep 30
                ((i++))
        done
```

Is seems to work, but it is clearly a hackish to continue the development. 
It's easy that this hardcoding is erased in a package update of the package.

## Design

## Class Diagram

```mermaid
classDiagram
    class Device_5Impl{
        <<tango>>
        +Init()
        +State()
        +Status()
    }
    class DLLRFBase{
        #AlbaStruck card
        #CardReader reader
        #AttributeInterpreter interpreter
        #ProcessStatistics statistics
        -Event _join_event
        -Thread _internal_thread
        +String Version_DeviceServer
        #float internal_polling
        #int time_arrays_max_length
        #List<float> internal_procedure_times
        #float internal_procedure_times_min
        #float internal_procedure_times_mean
        #float internal_procedure_times_std
        #float internal_procedure_times_max
        #List<float> read_registers_time
        #float read_registers_time_min
        #float read_registers_time_mean
        #float read_registers_time_std
        #float read_registers_time_max
        #List<float> process_registers_time
        #float process_registers_time_min
        #float process_registers_time_std
        #float process_registers_time_mean
        #float process_registers_time_max
        #List<float> internal_formulas_time
        #float internal_formulas_time_min
        #float internal_formulas_time_mean
        #float internal_formulas_time_std
        #float internal_formulas_time_max
        #List<int> events_emitted
        #float events_emitted_min
        #float events_emitted_mean
        #float events_emitted_std
        #float events_emitted_max
        +Init(string BoardModel, int BoardId, int WriteOffset)
        +Exec(string): string
        +Start(): boolean
        +Stop(): boolean
        #_fpga_initialization()
        #_change_state_status(tangoDevState, String, bool, bool)
        -__process_new_state(tangoDevState): tangoDevState
        -__process_new_status(String, bool, bool): String
        #create_attribute(int address, String attribute, Access access, 
                          AttrType att_type, DataType att_dtype, 
                          String unit, numerical min_value, 
                          numerical max_value, String w_formula, 
                          String r_formula)
        #read_Attribute(tangoAttribute, String)
        #write_Attribute(tangoAttribute, String)
        #is_Attribute_allowed(tangoAttribute, String): bool
        -_get_internal_attribute_obj(String): Attribute
        -_get_attribute_read_value(Attribute): numerical
        -_get_attribute_write_value(tangoAttribute): numerical
        -_apply_write_formula(Attribute, numerical)
        -_do_write(Attribute, numerical)
        -_internal_procedures()
        -_read_all_hw_attributes()
        -_refresh_calculated_attributes()
    }
    class DLLRF{
        +String CSVInfo
        +String Version_CSV
        +String Version_CodeGenerator
    }
    class enum{
        <<python>>
    }
    class ExtendedEnum
    class Access{
        read = 0
        read_write = 1
    }
    class AttrType{
        settings = 0
        diags = 1
    }
    class DataType{
        bool = 0
        short = 1
        long = 2
        float = 3
        string = 4
        volts = 10
        angle = 11
    }
    class Attribute{
        +int address
        +String name
        +Access access
        +AttrType attr_type
        +DataType data_type
        +String unit
        +numeric min_value
        +numeric max_value
        +String fread
        +String fwrite
        +numeric value
        +datetime last_update
        +float last_update_float
        set_value(value, ts)
    }
    class AlbaStruck{
        -Card card
        #int write_offset
        +read_register(int number): int
        +read_registers(List<int> numbers): List<int>
        +write_register(int number, int value, bool confirmation, int force_write_offset)
    }
    class CardReader{
        -Card card
        +read(List<int>): dict<int, int>, deltatime
    }
    class AttributeInterpreter{
        -tango_device device
        -tango_multiattr multiattribute
        #dict attributes_dict
        +process_read_data(List<int>, ts): int, int, deltatime
        +process_internal_formulas(dict<Attribute>): int, int, deltatime
        #apply_read_formula(Attribute, int): numerical
        #cast_read_value(Attribute, numerical): numerical
    }
    class ProcessStatistics{
        #int time_arrays_max_length
        #List<float> internal_procedure_times
        #List<float> read_registers_time
        #List<float> process_registers_time
        #List<float> internal_formulas_time
        #List<int> events_emitted
        -__append_to_array(List, value): List
    }
    class Card{
        <<python-struck.sis830x>>
        -__init__(self, device_or_index, last_register=0xfff)
        +open()
        +close()
        +read_register(int): int
        +read_registers(List<int>): List<int>
        +write_register(int, int, bool): int
        +write_registers(List<int, int>, bool): List<int>
    }
    class AbstractInitialization{
        -Device_5Impl __device
        +Device_5Impl device
    }
    class list{
        <<python>>
    }
    class InitializationGroup{
        -string __group_name
        +append(AbstractInitialization item)
        +insert(int idx, AbstractInitialization item)
        +apply()
    }
    class InitializationAction{
        -boolean dry_run
    }
    class RegisterWrite{
        -string __human_meaning
        -string __device_filename
        -int __register_address
        -int __register_value
        +apply()
    }
    class AttributeWrite{
        +string __attribute_name
        +numeric __attribute_value
        #TangoMultiattr __multiattr_obj
        +apply()
    }
    class SystemExecution{
        -string __script_name
        -list<string> __script_arguments
    }

    Device_5Impl <|-- DLLRFBase
    DLLRFBase <|-- DLLRF
    
    AlbaStruck *-- Card
    DLLRFBase --> AlbaFpga
    DLLRFBase *-- AlbaStruck
    DLLRFBase *-- CardReader
    DLLRFBase *-- AttributeInterpreter
    DLLRFBase *-- ProcessStatistics
    DLLRF *-- Attribute: "0..n"
    DLLRFBase *-- InitializationGroup: "fpga_initialization_procedure()"
    
    enum <|-- ExtendedEnum
    ExtendedEnum <|-- Access
    ExtendedEnum <|-- AttrType
    ExtendedEnum <|-- DataType
    
    list <|-- InitializationGroup
    AbstractInitialization <|-- InitializationGroup
    AbstractInitialization <|-- InitializationAction
    InitializationAction <|-- RegisterWrite
    InitializationAction <|-- AttributeWrite
    InitializationAction <|-- SystemExecution
```

## Procedures

### Start()

Procedure described by the fpga programmer:

```mermaid
sequenceDiagram
    participant User
    participant Device
    participant FPGA initialization
    participant Port 12 Communication enable
    participant VCXO programming
    participant Set external clock
    participant RegisterWrite
    participant AttributeWrite
    participant SystemExecution
    
    User ->> Device: Start()
    Device ->> Device: thread creation
    Device ->> Device: _change_state_status(STANDBY)
    Device ->> User: ack
    
    Device ->> FPGA initialization: apply()
    FPGA initialization ->> Port 12 Communication enable: apply()
    Port 12 Communication enable ->> RegisterWrite: apply("Disable port 12&13 backplane communication")
    Port 12 Communication enable ->> RegisterWrite: apply("DIO output from Backplane port 12")
    Port 12 Communication enable ->> RegisterWrite: apply("Enable port12 backplane communication")
    Port 12 Communication enable ->> RegisterWrite: apply("DIO1 output and DIO2 input")
    Port 12 Communication enable ->> SystemExecution: ~/SIS/sis8300KU_tests/sfp-port-link-tests/init_mgt_clks
    Port 12 Communication enable ->> SystemExecution: ~/SIS/sis8300KU_tests/port_link_test/port_link_test /dev/sis8300-s4 0x1
    FPGA initialization ->> VCXO programming: apply()
    VCXO programming ->> AttributeWrite: apply(Mux0_Divider, 1)
    VCXO programming ->> AttributeWrite: apply("Mux1_Divider", 4)
    VCXO programming ->> AttributeWrite: apply("Y0enable", 1)
    VCXO programming ->> AttributeWrite: apply("Y1enable", 1)
    VCXO programming ->> AttributeWrite: apply("VCXO_Outputs_enable", 1)
    VCXO programming ->> AttributeWrite: apply("Send_Word", 0)
    VCXO programming ->> AttributeWrite: apply("Send_Word", 1)
    VCXO programming ->> AttributeWrite: apply("Send_Word", 0)
    FPGA initialization ->> Set external clock: apply()
    Set external clock ->> RegisterWrite: apply("Front panel SMA clock")
    Set external clock ->> RegisterWrite: apply("DAC set to complement 2")
    Device ->> Device: _change_state_status(RUNNING)
```

### internal poll

The refresh of the information from the fpga starts with a read of all the 
registers as close as possible in time. Then those values are stored in the 
internal structure to then complete the refresh with the internal structures 
that doesn't have an explicit register but a reference to one or more of them.

```mermaid
sequenceDiagram
    Device ->> Device: Init()
    Device ->> InternalThread: Start()
    loop while not JoinEvent is Set
        InternalThread ->> Device: _read_all_hw_attributes
        Device ->> Reader: read(registers)
        Device ->> Interpreter: process_read_data(List<int>, timestamp)
        InternalThread ->> Device: _refresh_calculated_attributes
        Device ->> Interpreter: process_internal_formulas(List<Attribute>)
    end
```

## Troubleshooting

### internal procedures

There is a loop that performs internal tasks, like the read of the registers of
the card and push the events of the attributes that have a newer value.

This internal loop provides some information and configuration that the 
expert user may like to check.

* internal_polling: strictly positive float with the time in seconds that 
  the loop will be repeating. 
* time_arrays_max_length: maximum length of the arrays with the previous 
  loop executions.
* internal_procedure_times: how much time has been used to complete all the 
  tasks in the internal procedure.
* read_registers_time: how much time has been used to perform the read 
  action over all the registers in the card
* process_registers_time: how much time has been used to, ones have new 
  values of the registers, fill the internal structures, see which of them 
  have change and emit the necessary tango events.
* events_emitted: number of events emitted by the attributes that have 
  change its value on each of the update loops.

(Note: the time arrays come with scalar attributes with the min, mean, std 
and max values in those arrays).

To see those values, to know if there is something wrong or simply by 
curiosity, one can read them using jive. To see them in a gui, it is 
necessary to have the conda environment for taurus provided by the 
[taurus-struck](https://gitlab.com/srgblnch-tangocs/struck/taurus-struck) package.

```bash
conda activate taurus-struck
taurus plot besy/rf/struck-01/{internal_procedure_times,read_registers_time,process_registers_time,internal_formulas_time,events_emitted} & 
taurus form besy/rf/struck-01/{{internal_procedure_times,read_registers_time,process_registers_time,internal_formulas_time}_{min,mean,std,max},internal_polling,time_arrays_max_length,events_emitted_{min,mean,std,max}} &

```
